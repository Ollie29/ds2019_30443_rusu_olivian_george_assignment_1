import { Injectable } from '@angular/core';
import { CaregiverInterface } from './interfaces/caregiver';

@Injectable()
export class CaregiverModel {

    all: Array<CaregiverInterface>;
    selectedCaregiverId: string;

    setSelected(id: string) {
        return this.all.find((caregiver: CaregiverInterface) => caregiver.id === id);
    }

    removeSelected() {
        this.selectedCaregiverId = undefined;
    }

    clear() {
        this.removeSelected();
        this.all = undefined;
    }

}