import { Injectable } from '@angular/core';
import { PatientInterface } from './interfaces/patient';
import { TouchSequence } from 'selenium-webdriver';

@Injectable()
export class PatientModel {

    all: Array<PatientInterface>;
    selectedPatientId: string;

    setSelected(id: string) {
        return this.all.find((patient: PatientInterface) => patient.id === id);
    }

    removeSelected() {
        this.selectedPatientId = undefined;
    }

    clear() {
        this.removeSelected();
        this.all = undefined;
    }

}