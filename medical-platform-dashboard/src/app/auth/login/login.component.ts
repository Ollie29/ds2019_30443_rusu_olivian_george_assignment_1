import { Component, OnInit, OnDestroy } from '@angular/core';
import { StorageService } from 'src/app/shared/services/storage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Subscription } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  username: string;
  password: string;

  constructor(private storageService: StorageService, private router: Router, 
    private route: ActivatedRoute, private authService: AuthService) { }

  ngOnInit() {
      this.subscription = this.route.queryParams.subscribe();

      if(this.authService.isLoggedIn()) {
        this.router.navigate(['/']);
      }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  handleLogin() {
    let authCode : string = btoa(`${this.username}:${this.password}`);
    console.log(this.username);
    this.authService.login(this.username, this.password, authCode).subscribe(
      (response: HttpResponse<any>) => {
          this.storageService.set(this.storageService.appToken, authCode);
          if((<any>response).authorities[0].authority === 'ROLE_DOCTOR') {
            this.router.navigate(['/doctor']);
          }
          else if((<any>response).authorities[0].authority === 'ROLE_PATIENT') {
            this.router.navigate(['/patient']);
          }
          else if((<any>response).authorities[0].authority === 'ROLE_CAREGIVER') {
              this.router.navigate(['/caregiver']);
          }
      }
    );
  }

}
