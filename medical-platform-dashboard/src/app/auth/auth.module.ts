import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { StorageService } from '../shared/services/storage.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthRoutingModule } from './auth-routing.module';
import { FormsModule } from '@angular/forms';


@NgModule({
    imports: [CommonModule, FormsModule, AuthRoutingModule, HttpClientModule],
    declarations: [LoginComponent],
    providers: [StorageService],
    exports: []
})
export class AuthModule {}