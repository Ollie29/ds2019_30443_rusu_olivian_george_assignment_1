import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PatientModel } from './shared/models/patient.model';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CaregiverModel } from './shared/models/caregiver.model';
import { MedicationModel } from './shared/models/medication.model';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      preventDuplicates: true
    }),
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [PatientModel, CaregiverModel, MedicationModel],
  bootstrap: [AppComponent]
})
export class AppModule { }
