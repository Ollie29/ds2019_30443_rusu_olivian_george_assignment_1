import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api.service';
import { CaregiverModel } from 'src/app/shared/models/caregiver.model';
import { ToastrService } from 'ngx-toastr';
import { CaregiverInterface } from 'src/app/shared/models/interfaces/caregiver';

@Component({
  selector: 'app-caregiver-insert',
  templateUrl: './caregiver-insert.component.html',
  styleUrls: ['./caregiver-insert.component.css']
})
export class CaregiverInsertComponent implements OnInit {

  caregiver: CaregiverInterface = {} as CaregiverInterface;

  constructor(private router: Router, private apiService: ApiService, private route: ActivatedRoute, private caregiverModel: CaregiverModel, private toastr: ToastrService) { }

  ngOnInit() {
  }

  onCaregiverCreate() {
     this.caregiver.id = '0';
     this.apiService.insertCaregiver(this.caregiver).subscribe(
       () => {
         this.toastr.success("Caregiver inserted successfuly!");
       }
     );
  }

}
