import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api.service';
import { CaregiverModel } from 'src/app/shared/models/caregiver.model';
import { ToastrService } from 'ngx-toastr';
import { CaregiverInterface } from 'src/app/shared/models/interfaces/caregiver';

@Component({
  selector: 'app-caregiver-details',
  templateUrl: './caregiver-details.component.html',
  styleUrls: ['./caregiver-details.component.css']
})
export class CaregiverDetailsComponent implements OnInit {

  caregiver: CaregiverInterface = {} as CaregiverInterface;
  private id: string;

  constructor(private router: Router, private apiService: ApiService, private route: ActivatedRoute, private caregiverModel: CaregiverModel, private toastr: ToastrService) { }

  ngOnInit() {
    this.route.params
      .subscribe(
          (params : Params) => {
            this.id = params['id'];
          }
      );
    this.getCaregiverByID();
  }

  getCaregiverByID(): void {
    this.apiService.getCaregiverById(this.id).subscribe(
        (data: CaregiverInterface) => {this.caregiver = data; } 
    );
}

onCaregiverSave(): void {
    this.apiService.saveCaregiverData(this.caregiver).subscribe(
      () => {
        this.toastr.success('Caregiver updated!');
      }
    );
}

onCaregiverDelete(): void {
  this.apiService.deleteCaregiver(this.id).subscribe(
    () => {
      this.toastr.success('Caregiver removed!');
    }
  );
    this.router.navigate(['/caregivers']);
  }
  


}
