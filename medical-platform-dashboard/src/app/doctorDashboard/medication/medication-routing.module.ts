import {Routes, RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import { MedicationListComponent } from './medication-list/medication-list.component';
import { MedicationDetailsComponent } from './medication-details/medication-details.component';
import { MedicationInsertComponent } from './medication-insert/medication-insert.component';

const routes: Routes = [
   { path: '', component: MedicationListComponent },
   { path: ':id/edit', component: MedicationDetailsComponent },
   { path: 'insert', component: MedicationInsertComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MedicationRoutingModule {}