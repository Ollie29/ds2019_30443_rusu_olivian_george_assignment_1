import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from '../../shared/services/api.service';
import { FormsModule } from '@angular/forms';
import { MedicationListComponent } from './medication-list/medication-list.component';
import { MedicationRoutingModule } from './medication-routing.module';
import { MedicationDetailsComponent } from './medication-details/medication-details.component';
import { MedicationInsertComponent } from './medication-insert/medication-insert.component';


@NgModule({
  declarations: [MedicationListComponent, MedicationDetailsComponent, MedicationInsertComponent],
  imports: [
    CommonModule,
    MedicationRoutingModule,
    FormsModule
  ],
  providers: [
    ApiService
  ]
})
export class MedicationModule { }
