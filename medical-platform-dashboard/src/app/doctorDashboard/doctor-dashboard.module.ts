import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthGuard } from "../shared/services/auth.guard.service";
import { AuthService } from "../shared/services/auth.service";
import { StorageService } from "../shared/services/storage.service";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { DoctorDashboardRoutingModule } from "./doctor-dashboard-routing.module";
import { AuthInterceptorService } from "../shared/interceptor/http.interceptor";
import { DoctorDashboardHeaderComponent } from './header/header.component';
import { DoctorDashboardMainComponent } from './main/main.component';


@NgModule({
    declarations: [DoctorDashboardHeaderComponent, DoctorDashboardMainComponent],
    imports: [CommonModule, HttpClientModule, DoctorDashboardRoutingModule],
    providers: [
        AuthGuard,
        AuthService,
        StorageService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptorService,
            multi: true
        }
    ]
})
export class DoctorDashboardModule {}