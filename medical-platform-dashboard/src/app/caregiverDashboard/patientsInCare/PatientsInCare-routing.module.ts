import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientsInCareListComponent } from './patients-in-care-list/patients-in-care-list.component';

const routes: Routes = [
    { path: '', component: PatientsInCareListComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PatientsInCareRoutingModule {}