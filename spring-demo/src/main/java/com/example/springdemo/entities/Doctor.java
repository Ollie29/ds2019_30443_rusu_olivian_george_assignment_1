package com.example.springdemo.entities;


import com.example.springdemo.entities.enums.DoctorType;

import javax.persistence.*;

@Entity
@Table(name = "doctor")
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "doctor_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private DoctorType doctorType;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Doctor() {
    }

    public Doctor(String firstName, String lastName, DoctorType doctorType, User user) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.doctorType = doctorType;
        this.user = user;
    }

    public Doctor(Integer id, String firstName, String lastName, DoctorType doctorType) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.doctorType = doctorType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public DoctorType getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(DoctorType doctorType) {
        this.doctorType = doctorType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", doctorType=" + doctorType +
                ", user=" + user +
                '}';
    }
}
