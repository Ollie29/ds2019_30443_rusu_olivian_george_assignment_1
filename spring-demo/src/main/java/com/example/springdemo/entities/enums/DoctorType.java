package com.example.springdemo.entities.enums;

public enum DoctorType {
    CARDIOLOGIST,
    DERMATOLOGIST,
    ENDOCRINOLOGIST,
    GERIATRIC
}
