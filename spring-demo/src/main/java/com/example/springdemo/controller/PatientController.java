package com.example.springdemo.controller;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/id/{id}")
    public PatientDTO getPatientById(@PathVariable("id") int id) {
        return patientService.getPatientById(id);
    }

    @GetMapping
    public List<PatientDTO> getAllPatients() {
        return patientService.getAll();
    }

    @GetMapping(value = "/firstName/{firstName}")
    public List<PatientViewDTO> getAllByFirstName(@PathVariable("firstName") String firstName){
        return patientService.getPatientsByFirstName(firstName);
    }

    @PostMapping(value = "/insert/{caregiverID}")
    public PatientDTO insertPatient(@RequestBody PatientDTO patientDto, @PathVariable("caregiverID") Integer caregiverID) {
        return patientService.insertPatient(patientDto, caregiverID);
    }

    @PutMapping(value = "/update/{caregiverID}")
    public Integer updatePatient(@RequestBody PatientDTO patientDTO, @PathVariable("caregiverID") Integer caregiverID) {
        return patientService.updatePatient(patientDTO, caregiverID);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deletePatient(@PathVariable("id") Integer id) {
        patientService.deletePatient(id);
    }
}
