package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationPlanDTO;
import com.pilldispenser.grpc.Medplan;
import com.pilldispenser.grpc.medicationPlanGrpc;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedPlanGRPCService extends medicationPlanGrpc.medicationPlanImplBase {


    private MedicationPlanService medicationPlanService;

    @Autowired
    public MedPlanGRPCService(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @Bean
    public void startServer() throws IOException, InterruptedException {
        Server server = ServerBuilder.forPort(9090).addService(new MedPlanGRPCService(this.medicationPlanService)).build();
        server.start();
        System.out.println("gRPC server started at port: "+server.getPort());
    }

    @Override
    public void getMedPlan(Medplan.MedPlanRequest request, StreamObserver<Medplan.MedPlanResponse> responseObserver) {
        Integer patientID = request.getPatientId();
        System.out.println(patientID);
        List<MedicationPlanDTO> medicationPlanDTOS = medicationPlanService.getAllMedPlans(patientID);

        Collection<Medplan.MedPlanResponse.MedicationPlan> medPlans = medicationPlanDTOS.stream()
                .map(x -> Medplan.MedPlanResponse.MedicationPlan.newBuilder()
                        .setId(x.getId())
                        .setMedPlanStartDate(x.getStartDate())
                        .setMedPlanEndDate(x.getEndDate())
                        .setIntakeInterval(x.getDailyIntakeInterval())
                        .addAllMedication(x.getMedicationListDTO().stream().map(y -> Medplan.MedPlanResponse.MedicationPlan.Medication.newBuilder()
                                .setName(y.getName())
                                .setDosage(y.getDosage())
                                .build()).collect(Collectors.toList()))
                .build()).collect(Collectors.toList());

        Medplan.MedPlanResponse response = Medplan.MedPlanResponse.newBuilder()
                .addAllMedPlan(medPlans)
                .build();

        responseObserver.onNext(response);

        responseObserver.onCompleted();
    }
}

