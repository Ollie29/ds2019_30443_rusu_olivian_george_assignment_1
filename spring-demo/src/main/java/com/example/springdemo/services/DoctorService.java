package com.example.springdemo.services;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.builders.DoctorBuilder;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DoctorService {

    private DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public DoctorDTO getDoctorById(Integer id) {
        Optional<Doctor> doctor  = doctorRepository.findById(id);
        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "doctor id", id);
        }
        return DoctorBuilder.generateDTOFromEntity(doctor.get());
    }

    public Integer insertDoctor(DoctorDTO doctorDTO) {
        Optional<Doctor> doctor = doctorRepository.findById(doctorDTO.getId());
        if(doctor.isPresent()) {
            throw new DuplicateEntryException("Doctor", "id", doctorDTO.getId().toString());
        }
        return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getId();
    }
}
