package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.entities.Caregiver;

public class CaregiverBuilder {

    public CaregiverBuilder() {
    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver) {
        return new CaregiverDTO(
                caregiver.getId(),
                caregiver.getFirstName(),
                caregiver.getLastName(),
                caregiver.getBirthDate(),
                caregiver.getGenderType(),
                caregiver.getAddress()
        );
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO dto) {
        return new Caregiver(
                dto.getId(),
                dto.getFirstName(),
                dto.getLastName(),
                dto.getBirthDate(),
                dto.getGender(),
                dto.getAddress()
        );
    }
}
