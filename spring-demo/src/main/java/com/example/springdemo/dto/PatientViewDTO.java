package com.example.springdemo.dto;

import com.example.springdemo.entities.enums.GenderType;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class PatientViewDTO {

    private String firstName;
    private String lastName;
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private String birthDate;
    private GenderType gender;
    private String address;

    public PatientViewDTO() {
    }

    public PatientViewDTO(String firstName, String lastName, String birthDate, GenderType gender, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public GenderType getGender() {
        return gender;
    }

    public void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
