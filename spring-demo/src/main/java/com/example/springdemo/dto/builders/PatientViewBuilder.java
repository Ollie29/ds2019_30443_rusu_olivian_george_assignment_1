package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.entities.Patient;

public class PatientViewBuilder {

    private PatientViewBuilder(){}

    public static PatientViewDTO generateDTOFromEntity(Patient patient){
        return new PatientViewDTO(
                patient.getFirstName(),
                patient.getLastName(),
                patient.getBirthDate(),
                patient.getGender(),
                patient.getAddress()
        );
    }

    public static Patient generateEntityFromDTO(PatientViewDTO patientViewDTO) {
        return new Patient(
                patientViewDTO.getFirstName(),
                patientViewDTO.getLastName(),
                patientViewDTO.getBirthDate(),
                patientViewDTO.getGender(),
                patientViewDTO.getAddress()
        );
    }
}
