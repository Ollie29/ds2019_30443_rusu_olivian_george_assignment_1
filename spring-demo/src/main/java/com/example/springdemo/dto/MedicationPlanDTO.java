package com.example.springdemo.dto;
import java.util.List;

public class MedicationPlanDTO {

    private Integer id;
    private String startDate;
    private String endDate;
    private List<MedicationDTO> medsDTO;
    private Integer dailyIntakeInterval;

    public MedicationPlanDTO(Integer id, String startDate, String endDate, List<MedicationDTO> medsDTO, Integer dailyIntakeInterval) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.medsDTO = medsDTO;
        this.dailyIntakeInterval = dailyIntakeInterval;
    }

    public MedicationPlanDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public List<MedicationDTO> getMedicationListDTO() {
        return medsDTO;
    }

    public void setMedsDTO(List<MedicationDTO> medsDTO) {
        this.medsDTO = medsDTO;
    }

    public Integer getDailyIntakeInterval() {
        return dailyIntakeInterval;
    }

    public void setDailyIntakeInterval(Integer dailyIntakeInterval) {
        this.dailyIntakeInterval = dailyIntakeInterval;
    }
}
