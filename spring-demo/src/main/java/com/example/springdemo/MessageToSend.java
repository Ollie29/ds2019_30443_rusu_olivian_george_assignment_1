package com.example.springdemo;

public class MessageToSend {
    private String message;

    public MessageToSend(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
